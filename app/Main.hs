{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -Wno-missing-deriving-strategies #-}

-- |
-- Copyright: (c) 2021 Fabien Boucher
-- SPDX-License-Identifier: MIT
-- Maintainer: Fabien Boucher <fboucher@redhat.com>
module Main (main) where

import Data.Aeson (decode)
import qualified Data.Yaml as Y
import qualified Data.Yaml.Pretty as YP
import Distgit2resources
  ( SFProject (..),
    SFRTopLevel,
    addReposToProject,
    createSFResources,
    createSFSROption,
    getGroupProjects,
    isProjectNamePrefixedWith,
    transformGLProjectToSRs,
  )
import GitLab.Types (GitLabServerConfig (..), Project (project_path_with_namespace, _links), defaultGitLabServer)
import Options.Generic (ParseRecord, Unwrapped, Wrapped, unwrapRecord, (:::), type (<?>))

data POptions w = POptions
  { -- input :: w ::: FilePath <?> "Path to the input yaml file",
    -- baseosinput :: w ::: FilePath <?> "Path to the input baseos json file",
    output :: w ::: FilePath <?> "Path to the output yaml file"
  }
  deriving (Generic)

instance ParseRecord (POptions Wrapped)

deriving instance Show (POptions Unwrapped)

apiKeyEnvName :: String
apiKeyEnvName = "API_KEY"

-- read-api scopes is needed
apiKeyEnvError :: String
apiKeyEnvError = error $ toText apiKeyEnvName <> " environment not found"

redHatCentOSStreamRPMSGID :: Int
redHatCentOSStreamRPMSGID = 8794173

newtype CProject = CProject {unCProject :: Project}

instance Eq CProject where
  (==) a b = project_path_with_namespace (unCProject a) == project_path_with_namespace (unCProject b)

instance Ord CProject where
  (<=) a b = project_path_with_namespace (unCProject a) <= project_path_with_namespace (unCProject b)

main :: IO ()
main =
  do
    print ("Running process ... this may take a while" :: Text)
    outputPath <- unwrapRecord "distgits2resources"
    apiKey <- fromMaybe apiKeyEnvError <$> lookupEnv apiKeyEnvName
    let sc = defaultGitLabServer {url = "https://gitlab.com", token = toText apiKey}
    allProjects <- getGroupProjects redHatCentOSStreamRPMSGID sc
    let resources = createSFResources "centos-distgits" "centos" "Centos Stream distgits" "gitlab.com" $ transformGLProjectToSRs (createSFSROption "c9s") (sortProjects allProjects)
    writeFile (output (outputPath :: POptions Unwrapped)) $ decodeUtf8 $ yamlData resources
  where
    yamlData resources' = YP.encodePretty YP.defConfig resources'
    sortProjects :: [Project] -> [Project]
    sortProjects projects = unCProject <$> sort (CProject <$> projects)

-- pythonProjects projects' = filter (isProjectNamePrefixedWith "python-") projects'

-- main :: IO ()
-- main = do
--   outputPath <- unwrapRecord "distgits2resources"
--   inputPath <- unwrapRecord "distgits2resources"
--   inputbaseosPath <- unwrapRecord "distgits2resources"
--   inputR <- readFile (input (inputPath :: POptions Unwrapped))
--   inputBaseOS <- readFile (baseosinput (inputbaseosPath :: POptions Unwrapped))
--   let resourcesE = decodeResources $ encodeUtf8 inputR
--   let baseosM = decode $ encodeUtf8 inputBaseOS :: Maybe [Text]
--   case (resourcesE, baseosM) of
--     (Right resources', Just baseos) -> do
--       case addDistgits resources' $ baseosWithprefix baseos of
--         Just np -> do
--           let resources = createSFResources "centos-distgits" (tenant np) (description np) (connection np) (sourceRepositories np)
--           writeFile (output (outputPath :: POptions Unwrapped)) $ decodeUtf8 $ yamlData resources
--         _ -> pure ()
--     _ -> pure ()
--   where
--     decodeResources :: ByteString -> Either Y.ParseException SFRTopLevel
--     decodeResources inputR = Y.decodeEither' inputR :: Either Y.ParseException SFRTopLevel
--     addDistgits :: SFRTopLevel -> [Text] -> Maybe SFProject
--     addDistgits resources names = addReposToProject resources (createSFSROption "c9s") "centos-distgits" names
--     yamlData resources' = YP.encodePretty YP.defConfig resources'
--     baseosWithprefix = map ("redhat/centos-stream/rpms/" <>)
