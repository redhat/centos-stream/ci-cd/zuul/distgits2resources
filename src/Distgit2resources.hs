{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-missing-deriving-strategies #-}

-- |
-- Copyright: (c) 2021 Fabien Boucher
-- SPDX-License-Identifier: MIT
-- Maintainer: Fabien Boucher <fboucher@redhat.com>
--
-- See README for more info
module Distgit2resources
  ( transformGLProjectToSRs,
    getGroupProjects,
    createSFResources,
    createSFSROption,
    isProjectNamePrefixedWith,
    addReposToProject,
    SFRTopLevel,
    SFProject (..),
  )
where

import Data.Aeson (FromJSON, ToJSON, eitherDecode, object, parseJSON, toJSON, withObject, (.:), (.=))
import qualified Data.HashMap.Strict as HM (fromList, (!?))
import qualified Data.Text as T
import GitLab (createMergeRequest, runGitLab)
import GitLab.API.Groups (groupProjects')
import GitLab.Types (GitLabServerConfig (..), Project (project_name, project_path_with_namespace))

newtype SFSROptions = SFSROptions
  { defaultBranch :: Text
  }
  deriving (Show, Eq)

instance ToJSON SFSROptions where
  toJSON (SFSROptions defaultBranch') =
    object ["default-branch" .= defaultBranch']

instance FromJSON SFSROptions where
  parseJSON = withObject "SFSROptions" $ \v ->
    SFSROptions
      <$> v .: "default-branch"

type SFSR = HashMap Text SFSROptions

type SFSRs = [SFSR]

data SFProject = SFProject
  { tenant :: Text,
    description :: Text,
    connection :: Text,
    sourceRepositories :: SFSRs
  }
  deriving (Show)

instance ToJSON SFProject where
  toJSON (SFProject tenant' description' connection' srs) =
    object
      [ "tenant" .= tenant',
        "description" .= description',
        "connection" .= connection',
        "source-repositories" .= srs
      ]

instance FromJSON SFProject where
  parseJSON = withObject "SFProject" $ \v ->
    SFProject
      <$> v .: "tenant"
      <*> v .: "description"
      <*> v .: "connection"
      <*> v .: "source-repositories"

newtype SFResources = SFResources
  { projects :: HashMap Text SFProject
  }
  deriving (Show)

instance ToJSON SFResources where
  toJSON (SFResources projects') =
    object ["projects" .= projects']

instance FromJSON SFResources where
  parseJSON = withObject "SFResources" $ \v ->
    SFResources
      <$> v .: "projects"

newtype SFRTopLevel = SFRTopLevel
  { resources :: SFResources
  }
  deriving (Show)

instance ToJSON SFRTopLevel where
  toJSON (SFRTopLevel resources') =
    object ["resources" .= resources']

instance FromJSON SFRTopLevel where
  parseJSON = withObject "SFRTopLevel" $ \v ->
    SFRTopLevel
      <$> v .: "resources"

createSFSROption :: Text -> SFSROptions
createSFSROption = SFSROptions

getGroupProjects :: Int -> GitLabServerConfig -> IO [Project]
getGroupProjects groupID sc = do
  projectsE <- runGitLab sc (groupProjects' groupID)
  case projectsE of
    Right projects' -> pure projects'
    Left _ -> error "Unable to get group projects list"

transformGLProjectToSRs :: SFSROptions -> [Project] -> SFSRs
transformGLProjectToSRs srOptions projects' = createSRs' $ map getNameWithNamespace projects'
  where
    getNameWithNamespace :: Project -> Text
    getNameWithNamespace project = project_path_with_namespace project
    createSRs' = createSRs srOptions

createSRs :: SFSROptions -> [Text] -> SFSRs
createSRs srOptions = map createSREntry
  where
    createSREntry :: Text -> SFSR
    createSREntry pname = HM.fromList [(pname, srOptions)]

createSFResources :: Text -> Text -> Text -> Text -> SFSRs -> SFRTopLevel
createSFResources projectName tenantName pDescription pConnection sfsrs =
  SFRTopLevel $
    SFResources $
      HM.fromList [(projectName, SFProject tenantName pDescription pConnection sfsrs)]

getProject :: Text -> SFRTopLevel -> Maybe SFProject
getProject name resources' =
  case resources' of
    SFRTopLevel (SFResources projects') -> projects' HM.!? name
    _ -> Nothing

getProjectSRs :: Text -> SFRTopLevel -> SFSRs
getProjectSRs name resources' = maybe [] sourceRepositories pM
  where
    pM = getProject name resources'

addRepoToSRs :: SFSROptions -> SFSRs -> [Text] -> SFSRs
addRepoToSRs opts srs names = dedup $ news <> srs
  where
    news = createSRs opts names
    dedup :: SFSRs -> SFSRs
    dedup srs' = case srs' of
      [] -> []
      (x : xs) -> if x `elem` xs then dedup xs else x : dedup xs

addReposToProject :: SFRTopLevel -> SFSROptions -> Text -> [Text] -> Maybe SFProject
addReposToProject rt opts pname names =
  case getProject pname rt of
    Just project -> Just project {sourceRepositories = newSRs}
    Nothing -> Nothing
  where
    newSRs = addRepoToSRs opts (getProjectSRs pname rt) names

isProjectNamePrefixedWith :: Text -> Project -> Bool
isProjectNamePrefixedWith prefix project = prefix `T.isPrefixOf` project_name project
