# distgit2resources

[![Hackage](https://img.shields.io/hackage/v/distgit2resources.svg?logo=haskell)](https://hackage.haskell.org/package/distgit2resources)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

This tool creates the resources YAML file for SoftwareFactory based on the list of
distgits available in redhat/centos-stream/rpms group.

The current resource file is https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/blob/master/resources/centos-distgits.yaml.

This fix is needed for gitlab-haskell: https://gitlab.com/robstewart57/gitlab-haskell/-/merge_requests/10

## Run the tool

Note that the "api_read" scope is needed for the token. The token is a "Personal Access Tokens".

```Shell
$ cabal build
$ API_KEY=<token> cabal run distgit2resources -- --output resources/centos-distgits.yaml
```
