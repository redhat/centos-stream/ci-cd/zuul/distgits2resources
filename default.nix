let
  # Fetch the nixpkgs repository                                                                                                                                                                                                                                          
  nixpkgs = (fetchTarball {
    url =
      "https://github.com/NixOS/nixpkgs/archive/a5eee47d7f49c1b629b100fa657c545153f40ba1.tar.gz";
    sha256 = "0zh9ky1830xbahyi3cv99v03n1mgkj90474nsbni1hzqal2zf3bk";
  });
  pkgs = import nixpkgs { };

  # Extend the haskell package set with a custom gitlab-haskell version                                                                                                                                                                                                   
  hsPkgs = pkgs.haskellPackages.extend (self: super: {
    gitlab-haskell = pkgs.haskell.lib.overrideCabal super.gitlab-haskell {
      src = pkgs.fetchFromGitLab {
        owner = "robstewart57";
        repo = "gitlab-haskell";
        rev = "8f612c6224dc5d0c627e6349c5eac43427be222e";
        sha256 = "13y4japw3z7vxlz8kkdxr485fz4sdfm6x0j6ynwdgvd0za3dzniv";
      };
    };
    relude = pkgs.haskell.lib.overrideCabal super.relude {
      version = "1.0.0.1";
      sha256 = "0cw9a1gfvias4hr36ywdizhysnzbzxy20fb3jwmqmgjy40lzxp2g";
    };
  });

  # Create a derivation for distgit2resources                                                                                                                                                                                                                             
  drv = hsPkgs.callCabal2nix "distgit2resources" ./. { };

  # Create a development environment for distgit2resources                                                                                                                                                                                                                
  shellDrv = hsPkgs.shellFor {
    withHoogle = true;
    packages = p: [ drv ];
    buildInputs = with hsPkgs; [
      hlint
      cabal-install
      haskell-language-server
      ghcid
    ];
  };

in shellDrv


